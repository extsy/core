FROM node:16-alpine3.12 as base

WORKDIR /usr/src/app

RUN apk update && apk add bash

COPY . .

RUN npm install

FROM base as build
RUN npm run build

RUN ls

FROM node:16-alpine3.12 as production
WORKDIR /usr/src/app

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

ARG PORT=3000
ENV PORT=${PORT}

COPY --from=build /usr/src/app/package*.json ./
COPY --from=build /usr/src/app/dist ./dist
RUN npm install --production

RUN ls -la

CMD ["node", "dist/src/main"]


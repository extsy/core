```typescript
import { webServer } from "../libs/server/server";
import { UserController } from "./controllers/users/users.controller";
import { TestClass } from "./controllers/testClass";

async function bootstrap() {
  webServer.controllers = [UserController];
  webServer.start();
}

bootstrap();
```

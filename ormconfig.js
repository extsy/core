require('dotenv').config()
function getEnv(v) {
    return process.env['MYSQL_' + v.toUpperCase()]
}

module.exports = {
    type: 'mysql',
    host: getEnv('host'),
    port: getEnv('port') || 3306,
    database: getEnv('database'),
    username: getEnv('user'),
    password: getEnv('password'),
    migrationsRun: true,
    synchronize: false,
    migrationsDir: 'migrations',
    autoLoadEntities: true,
    entities: ['dist/**/*.entity.js',],
    migrations: ["dist/migrations/*.js"],
    cli: {
      migrationsDir: "migrations"
    },

}


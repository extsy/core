import { NextFunction, Request, RequestHandler, Response } from 'express'
import { HttpError, InternalServerError, Unauthorized } from 'http-errors'
import { Meta, RequestParameterMetadata } from '../decorators/metadata.enum'
import * as _ from 'lodash'
import { Validator } from '../validator/validator'
import { Guard } from '../guards/guard.interface'

interface ArgumentItem {
  index: number
  value: any
}

function handleBody(
  target: any,
  propertyKey: string,
  descriptor: Function,
  request: Request
): ArgumentItem[] {
  const args: ArgumentItem[] = []
  let bodyConfigMeta = Reflect.getMetadata(
    Meta.REQUEST_BODY,
    target,
    propertyKey
  ) as RequestParameterMetadata[]

  if (bodyConfigMeta) {
    bodyConfigMeta = bodyConfigMeta.sort(
      (a: RequestParameterMetadata, b: RequestParameterMetadata) =>
        a.index - b.index
    )

    for (let i in bodyConfigMeta) {
      const data = _.get(request, bodyConfigMeta[i].key)
      const typeClass = bodyConfigMeta[i].type

      const typeObject = new typeClass()
      if (data) {
        for (let k in data) {
          ;(typeObject as any)[k] = data[k]
        }
      }

      Validator.validate(typeObject)

      args.push({
        index: bodyConfigMeta[i].index,
        value: data
      })
    }
  }

  return args
}

export function setupController<T>(
  target: T,
  propertyKey: string,
  descriptor: Function,
  guard?: Guard
) {
  // verify if there is a guard
  const standardHandler = async (
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> => {
    descriptor = descriptor.bind(target)
    try {
      const authData = Reflect.getMetadata(Meta.ROLES, target, propertyKey)
      if (authData && guard && !guard.isAllowed(req)) {
        throw new Unauthorized()
      }
      const bodyArgs = handleBody(target, propertyKey, descriptor, req)

      const args: ArgumentItem[] = [...bodyArgs].sort(
        (a: ArgumentItem, b: ArgumentItem) => a.index - b.index
      )

      const result = await descriptor(
        ...args.map((arg: ArgumentItem) => arg.value)
      )

      res.status(200).json(result)
    } catch (e) {
      next(e)
    }
  }

  return standardHandler
}

export const createParameterDecotator: ParameterDecorator = (
  target: Object,
  propertyKey: string | symbol,
  parameterIndex: number
) => {}

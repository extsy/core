import { NextFunction, Request, RequestHandler, Response } from 'express'
import Container from 'typedi'
import { Meta, RequestParameterMetadata } from '../decorators/metadata.enum'
import { Guard } from '../guards/guard.interface'
import { HttpMethod } from '../server/server'
import { Unauthorized } from 'http-errors'
import { Validator } from '../validator/validator'
import * as _ from 'lodash'

interface ClassControllerMeta {
  target: any
  basePath: string
  controllers: string[]
}

interface ControllerMeta {
  handler: RequestHandler
  propertyKey: string
  path: string
  method: HttpMethod
}

interface ArgumentItem {
  index: number
  value: any
}

export class ControllerUtils {
  static get(classController: any): ClassControllerMeta {
    const target = Container.get(classController) as any
    let basePath = Reflect.getMetadata(Meta.CLASS_CONTROLLER, target)

    // Get class controllers
    const controllerNames = Reflect.getMetadata(
      Meta.CLASS_CONTROLLER_METHODS,
      target
    ) as string[]

    const meta: ClassControllerMeta = {
      target,
      basePath,
      controllers: controllerNames
    }

    return meta
  }

  static getController(
    classMeta: ClassControllerMeta,
    controller: string,
    routePrefix: string,
    guards: Guard[] = []
  ): ControllerMeta {
    const method: HttpMethod = Reflect.getMetadata(
      Meta.ROUTER_METHOD,
      classMeta.target,
      controller
    ) as HttpMethod

    let path = Reflect.getMetadata(
      Meta.ROUTER_PATH,
      classMeta.target,
      controller
    ) as string

    let fullPath = routePrefix + classMeta.basePath + path
    const meta = {
      method,
      propertyKey: controller,
      path: fullPath,
      handler: (() => {}) as RequestHandler
    }

    meta.handler = ControllerUtils.getRequestHandler(classMeta, meta, guards)
    return meta
  }

  private static async guardRoute(
    request: Request,
    classControllerMeta: ClassControllerMeta,
    controllerMeta: ControllerMeta,
    guards: Guard[] = []
  ) {
    const authData = Reflect.getMetadata(
      Meta.ROLES,
      classControllerMeta.target,
      controllerMeta.propertyKey
    )

    if (authData && guards.length > 0) {
      for (let i in guards) {
        if (!(await guards[i].isAllowed(request))) {
          throw new Unauthorized()
        }
      }
    }
  }

  private static getRequestHandler(
    classControllerMeta: ClassControllerMeta,
    controllerMeta: ControllerMeta,
    guards: Guard[] = []
  ) {
    let descriptor = classControllerMeta.target[controllerMeta.propertyKey]
    const target = classControllerMeta.target

    return async (
      req: Request,
      res: Response,
      next: NextFunction
    ): Promise<void> => {
      try {
        await ControllerUtils.guardRoute(
          req,
          classControllerMeta,
          controllerMeta,
          guards
        )

        descriptor = descriptor.bind(target)

        const bodyArgs = ControllerUtils.getBodyArguments(
          req,
          classControllerMeta,
          controllerMeta
        )

        const args: ArgumentItem[] = [...bodyArgs].sort(
          (a: ArgumentItem, b: ArgumentItem) => a.index - b.index
        )

        const result = await descriptor(
          ...args.map((arg: ArgumentItem) => arg.value)
        )

        res.status(200).json(result)
      } catch (e) {
        next(e)
      }
    }
  }

  private static getBodyArguments(
    request: Request,
    classMeta: ClassControllerMeta,
    controllerMeta: ControllerMeta
  ): ArgumentItem[] {
    const target = classMeta.target
    const propertyKey = controllerMeta.propertyKey
    const descriptor = target[propertyKey]

    const args: ArgumentItem[] = []
    let bodyConfigMeta = Reflect.getMetadata(
      Meta.REQUEST_BODY,
      target,
      propertyKey
    ) as RequestParameterMetadata[]

    if (bodyConfigMeta) {
      bodyConfigMeta = bodyConfigMeta.sort(
        (a: RequestParameterMetadata, b: RequestParameterMetadata) =>
          a.index - b.index
      )

      for (let i in bodyConfigMeta) {
        const data = _.get(request, bodyConfigMeta[i].key)
        const typeClass = bodyConfigMeta[i].type

        const typeObject = new typeClass()
        if (data) {
          for (let k in data) {
            ;(typeObject as any)[k] = data[k]
          }
        }

        Validator.validate(typeObject)

        args.push({
          index: bodyConfigMeta[i].index,
          value: data
        })
      }
    }

    return args
  }

  // static setupController<T>(
  //   target: T,
  //   propertyKey: string,
  //   descriptor: Function,
  //   guards: Guard[] = []
  // ) {
  //   // verify if there is a guard
  //   const standardHandler = async (
  //     req: Request,
  //     res: Response,
  //     next: NextFunction
  //   ): Promise<void> => {
  //     descriptor = descriptor.bind(target)
  //     try {
  //       const authData = Reflect.getMetadata(Meta.ROLES, target, propertyKey)
  //       if (
  //         authData &&
  //         guards.length > 0 &&
  //         !guards.find(async (guard: Guard) => await !guard.isAllowed(req))
  //       ) {
  //         throw new Unauthorized()
  //       }
  //       const bodyArgs = ControllerUtils.handleBody(
  //         target,
  //         propertyKey,
  //         descriptor,
  //         req
  //       )

  //       const args: ArgumentItem[] = [...bodyArgs].sort(
  //         (a: ArgumentItem, b: ArgumentItem) => a.index - b.index
  //       )

  //       const result = await descriptor(
  //         ...args.map((arg: ArgumentItem) => arg.value)
  //       )

  //       res.status(200).json(result)
  //     } catch (e) {
  //       next(e)
  //     }
  //   }

  //   return standardHandler
  // }
}

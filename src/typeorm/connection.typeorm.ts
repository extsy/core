import { Service } from 'typedi'
import { Connection, getConnection } from 'typeorm'

@Service()
export class TypeORM {
  private _connection!: Connection

  constructor() {
    this._connection = getConnection()
    console.log(this._connection)
  }

  get getRepository() {
    return this._connection.getRepository
  }
}

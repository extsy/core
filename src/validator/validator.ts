import { validate, validateSync, ValidationError } from 'class-validator'
import { UnprocessableEntity } from 'http-errors'

export class HttpValidationError extends UnprocessableEntity {
  errors: any = {}
  constructor(validationError: ValidationError) {
    super()
    if (validationError.constraints !== undefined) {
      this.errors[validationError.property] = Object.values(
        validationError.constraints
      )
    }
  }
}

export class Validator {
  static validate(obj: any) {
    const errors = validateSync(obj)
    if (errors.length > 0) {
      throw new HttpValidationError(errors[0])
    }
  }
}

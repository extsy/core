import Container from 'typedi'
import { EntitySchema } from 'typeorm'
import { TypeOrmPlugin } from './typeorm.plugin'

export const InjectRepo =
  (entity: Function | EntitySchema) =>
  (target: any, propertyKey: string | symbol) => {
    const plugin = Container.get(TypeOrmPlugin)
    target[propertyKey] = plugin
      .getRepository(entity)
      .then((repo: any) => (target[propertyKey] = repo))
  }

import { create } from 'lodash'
import {
  Connection,
  createConnection,
  EntitySchema,
  EntityTarget,
  getConnection,
  Repository
} from 'typeorm'
import { Plugin } from '../plugin.interface'

import { Service } from 'typedi'
import { InternalServerError } from 'http-errors'
import { LoggerService, InjectLogger } from '../../logger'

const Plugin = Service

@Plugin()
@InjectLogger()
export class TypeOrmPlugin implements Plugin {
  private _connection!: Connection

  private connectionPromise: Promise<Connection> | null = null

  private log!: LoggerService

  constructor() {
    this.init()
  }

  async init(): Promise<void> {
    if (!this._connection && !this.connectionPromise) {
      this.log.info('Connecting...')

      this.connectionPromise = createConnection()
      this._connection = await this.connectionPromise
      this.connectionPromise = null

      this.log.info('Connected!')
    }
  }

  public async getRepository<Entity>(
    entity: EntityTarget<Entity>
  ): Promise<Repository<Entity>> {
    if (this.connectionPromise !== null) {
      await this.connectionPromise
    }

    let name = entity.valueOf().toString()
    name = name.replace('class ', '')
    name = name.split(' ')[0].trim()
    this.log.debug(`Resolved repository for entity '${name}'`)
    return this._connection.getRepository(entity)
  }
}

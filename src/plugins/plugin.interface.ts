import { NotImplemented } from 'http-errors'

export interface Plugin {
  init(): void | Promise<void>
}

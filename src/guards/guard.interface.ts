import { Request } from 'express'

export interface Guard {
  isAllowed(request: Request): boolean
}

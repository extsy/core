import { Meta, RequestParameterMetadata } from './metadata.enum'

const getRequestProperty =
  (key: string) =>
  (target: Object, propertyKey: string | symbol, parameterIndex: number) => {
    let meta: RequestParameterMetadata[] = Reflect.getMetadata(
      Meta.REQUEST_BODY,
      target,
      propertyKey
    )

    if (!meta) {
      meta = []
    }

    const types = Reflect.getMetadata('design:paramtypes', target, propertyKey)
    meta.push({
      index: parameterIndex,
      type: types[parameterIndex],
      key: key
    })

    Reflect.defineMetadata(Meta.REQUEST_BODY, meta, target, propertyKey)
  }

export const Body = (key?: string): ParameterDecorator =>
  getRequestProperty(key ? 'body.' + key : 'body')
export const User = (): ParameterDecorator =>
  getRequestProperty('headers.authorization')

export const Roles =
  (auth: string): MethodDecorator =>
  (
    target: any,
    propertyKey: string | symbol,
    descriptor: PropertyDescriptor
  ) => {
    Reflect.defineMetadata(Meta.ROLES, auth, target, propertyKey)
  }

export enum Meta {
  ROUTER_PATH = 'ex:router:path',
  ROUTER_METHOD = 'ex:router:method',
  CLASS_CONTROLLER = 'ex:class:controller',
  CLASS_CONTROLLER_METHODS = 'ex:class:controller:methods',
  REQUEST_BODY = 'ex:body',
  ROLES = 'ex:roles'
}

export interface RequestParameterMetadata {
  /**
   * Which argument we're talking about
   */
  index: number

  /**
   * Argument type
   */
  type: any

  /**
   * request[key] we're parsing
   */
  key: string
}

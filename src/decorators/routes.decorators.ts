import { NextFunction, Request, RequestHandler, Response } from 'express'
import { HttpMethod, webServer } from '../server/server'
import { HttpError, InternalServerError } from 'http-errors'
import { Meta } from './metadata.enum'
import { Service } from 'typedi'
import * as _ from 'lodash'

const _routeConfig =
  (method: HttpMethod, path: string, callbackFunction: any) =>
  async (
    target: any,
    propertyKey: string | symbol,
    descriptor: PropertyDescriptor
  ) => {
    if (path.startsWith('/')) {
      path = path.substring(1, path.length)
    }

    Reflect.defineMetadata(Meta.ROUTER_PATH, path, target, propertyKey)
    Reflect.defineMetadata(
      Meta.ROUTER_METHOD,
      method.toLowerCase(),
      target,
      propertyKey
    )

    // Setup class controller
    let meta = Reflect.getMetadata(Meta.CLASS_CONTROLLER_METHODS, target)
    if (!meta) {
      Reflect.defineMetadata(
        Meta.CLASS_CONTROLLER_METHODS,
        [propertyKey],
        target
      )
    } else {
      meta.push(propertyKey)
      Reflect.defineMetadata(Meta.CLASS_CONTROLLER_METHODS, meta, target)
    }
  }

export const Get = (path: string = '/'): MethodDecorator => {
  return function (
    target: Object,
    propertyKey: string | symbol,
    descriptor: PropertyDescriptor
  ) {
    _routeConfig(HttpMethod.GET, path, descriptor)(
      target,
      propertyKey,
      descriptor
    )
  }
}

export const Post = (path: string = '/'): MethodDecorator => {
  return function (
    target: Object,
    propertyKey: string | symbol,
    descriptor: PropertyDescriptor
  ) {
    _routeConfig(HttpMethod.POST, path, descriptor)(
      target,
      propertyKey,
      descriptor
    )
  }
}

export const Delete = (path: string = '/'): MethodDecorator => {
  return function (
    target: Object,
    propertyKey: string | symbol,
    descriptor: PropertyDescriptor
  ) {
    _routeConfig(HttpMethod.DELETE, path, descriptor)(
      target,
      propertyKey,
      descriptor
    )
  }
}

export const Put = (path: string = '/'): MethodDecorator => {
  return function (
    target: Object,
    propertyKey: string | symbol,
    descriptor: PropertyDescriptor
  ) {
    _routeConfig(HttpMethod.PUT, path, descriptor)(
      target,
      propertyKey,
      descriptor
    )
  }
}

export const Controller = (path?: string) => (constructor: any) => {
  if (!path) {
    path = constructor.name.replace('Controller', '')
    path = _.camelCase(path)
  }

  if (path) {
    path = path.startsWith('/') ? path : '/' + path
    path = path.endsWith('/') ? path : path + '/'
  }

  Reflect.defineMetadata(Meta.CLASS_CONTROLLER, path, constructor.prototype)
  Service()(constructor)
}

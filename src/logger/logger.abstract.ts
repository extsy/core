import chalk, { ChalkFunction } from 'chalk'
import moment from 'moment'
import morgan from 'morgan'
import { LoggerInterface } from './logger.interface'
import { LoggerService } from './logger.service'
import { LogType } from './logtype.enum'

export abstract class Logger implements LoggerInterface {
  private _level: LogType = LogType.INFO

  constructor(public context: string = '') {
    require('dotenv').config()
    this.setLevel(process.env.LOG_LEVEL)
  }

  info(...args: any[]): void {
    throw new Error('Method not implemented.')
  }
  error(...args: any[]): void {
    throw new Error('Method not implemented.')
  }
  debug(...args: any[]): void {
    throw new Error('Method not implemented.')
  }
  warn(...args: any[]): void {
    throw new Error('Method not implemented.')
  }

  public drawSeparator(color: ChalkFunction = chalk.dim) {
    console.log(color('-'.repeat(process.stdout.columns)))
  }

  public setLevel(level: string | undefined): void {
    if (level === undefined) {
      this._level = LogType.INFO
      return
    }

    if (!Object.keys(LogType).includes(level.toUpperCase())) {
      throw new Error(`Invalid log level: ${level}`)
    }

    this._level = (LogType as any)[level.toUpperCase()]
  }

  clear() {
    console.clear()
  }

  static getTag(type: LogType, context: string, hideType = false): string {
    const timestamp = moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
    let tag = `[${timestamp}]`

    if (context) {
      tag += `[${context}]`
    }

    if (!hideType) {
      let _tag
      for (let i in LogType) {
        if ((LogType as any)[i] === type) {
          _tag = i
        }
      }

      tag += `[${_tag}]`
    }

    switch (type) {
      case LogType.INFO:
        tag = chalk.green(tag)
        break
      case LogType.DEBUG:
        tag = chalk.magenta(tag)
        break
      case LogType.ERROR:
        tag = chalk.redBright(tag)
        break
      case LogType.WARN:
        tag = chalk.yellow(tag)
        break
      case LogType.HTTP:
        tag = chalk.cyan(tag)
        break
    }

    return tag
  }

  protected print(type: LogType, ...args: any[]) {
    if (process.env.NODE_ENV !== 'test' && this._level >= type) {
      console.log(`\r${Logger.getTag(type, this.context)}`, ...args)
    }
  }

  static get HttpLogger() {
    const log = new LoggerService('Router')

    return morgan((tokens, req, res) => {
      return [
        Logger.getTag(LogType.HTTP, log.context, true),
        Logger.getStatusColored(tokens.status(req, res) as string),
        chalk.yellow(tokens.method(req, res)),
        tokens.url(req, res),
        // tokens.res(req, res, "content-length"),
        `(${tokens['response-time'](req, res)} ms)`
      ].join(' ')
    })
  }

  static getStatusColored(status: string | number): string {
    status = Number.parseInt(status as string)

    if (status < 400) {
      return chalk.green(status)
    }

    if (status < 500) {
      return chalk.yellow(status)
    }

    if (status >= 500) {
      return chalk.redBright(status)
    }
    return status.toString()
  }
}

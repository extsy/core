import { ChalkFunction } from 'chalk'

export interface LoggerInterface {
  context: string
  info(...args: any[]): void
  error(...args: any[]): void
  debug(...args: any[]): void
  warn(...args: any[]): void
}

import chalk, { ChalkFunction } from 'chalk'
import moment from 'moment'
import morgan from 'morgan'
import { Service } from 'typedi'
import { Logger } from './logger.abstract'
import { LoggerInterface } from './logger.interface'
import { LogType } from './logtype.enum'

@Service()
export class LoggerService extends Logger {
  warn(...args: any[]): void {
    this.print(LogType.WARN, ...args)
  }
  info(...args: any[]): void {
    this.print(LogType.INFO, ...args)
  }

  error(...args: any[]): void {
    this.print(LogType.ERROR, ...args)
  }

  debug(...args: any[]): void {
    this.print(LogType.DEBUG, ...args)
  }
}

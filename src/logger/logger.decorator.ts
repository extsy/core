import { LoggerService } from './logger.service'

export const InjectLogger = () => (constructor: any) => {
  constructor.prototype.log = new LoggerService(constructor.name)
}

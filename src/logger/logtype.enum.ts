export enum LogType {
  ERROR = '1',
  WARN = '2',
  INFO = '3',
  DEBUG = '4',
  HTTP = '5'
}

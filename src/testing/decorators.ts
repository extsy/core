import Container, { Service } from 'typedi'
import { Plugin } from '../plugins/plugin.interface'
const TEST_METADATA = 'ex:testMetadata'

// export const Test =
//   (testDescription: string) =>
//   (target: any, key: string, descriptor: PropertyDescriptor) => {
//     const func = descriptor.value
//     descriptor.value = it(testDescription, async () => {
//       await func.bind(target)()
//     })
//   }
// // export const BeforeAll =
// //   () => (target: any, key: string, descriptor: PropertyDescriptor) => {
// //     const func = descriptor.value
// //     descriptor.value = beforeAll(async () => {
// //       await func.bind(target)()
// //     })
// //   }

// export const TestModule = (customName?: string) => (constructor: any) => {
//   Service()(constructor)
// }

// export abstract class TestingModule {
//   protected plugins: (new (...args: any[]) => Plugin)[] = []

//   constructor() {
//     console.log('binding')
//     for (let i in this.plugins) {
//       Container.get(this.plugins[i]).init()
//     }
//   }
// }

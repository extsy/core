import 'reflect-metadata'
import '../helpers/prototypes'

import * as http from 'http'
import express, {
  Express,
  Handler,
  Response,
  Request,
  NextFunction
} from 'express'
import chalk from 'chalk'
import Container, { Service } from 'typedi'
import {
  HttpError,
  InternalServerError,
  NotFound,
  UnprocessableEntity
} from 'http-errors'
import { setupController } from '../helpers'
import { Meta } from '../decorators/metadata.enum'
import { LoggerService } from '../logger/logger.service'
import helmet from 'helmet'
import { readFileSync } from 'fs'
import { Env } from './environment.enum'
import { Logger } from '../logger/logger.abstract'
import { Guard } from '../guards/guard.interface'
import { LoggerInterface } from '../logger/logger.interface'
import { Plugin } from '../plugins/plugin.interface'
import { IController, IWebServer } from './server.interface'
import { ControllerUtils } from '../helpers/controller.utils'

export enum HttpMethod {
  GET = 'get',
  POST = 'post',
  PUT = 'put',
  DELETE = 'delete'
}

class HttpValidationError extends UnprocessableEntity {
  errors: any = {}
}

@Service()
export class WebServer implements IWebServer {
  private _app!: Express
  private _server!: http.Server

  controllers: Array<new (...args: any[]) => IController> = []
  middleware: Handler[] = []
  set guards(guards: (new (...args: any[]) => Guard)[]) {
    // TODO: handle setup guards
    for (let i in guards) {
      const guard = Container.get(guards[i]) as Guard
      this._guards.push(guard)
      this.log.debug(chalk.bold('Guard loaded:') + ' ' + guard.constructor.name)
    }
  }

  plugins: (new (...args: any[]) => Plugin)[] = []

  private _routePrefix: string = '/'

  private _guards: Guard[] = []

  private log!: Logger

  constructor() {
    require('dotenv').config()
    this.log = Container.get(LoggerService) as LoggerService
    this.log.context = 'WebServer'
    process.on('SIGINT', this.shutdown.bind(this))
    this.log.clear()
    this.showBanner()
    this._app = express()
    this._app.set('port', process.env.PORT || 3000)
  }

  set logger(logger: new (...args: any[]) => LoggerInterface) {
    this.log = Container.get(logger) as Logger
  }
  private showBanner() {
    const pkg = JSON.parse(
      readFileSync(process.cwd() + '/package.json', 'utf-8')
    )
    const log = new LoggerService('Bootstrap')
    const info = (label: string, content: string) => {
      log.info(chalk.bold(`${label}:`), content)
    }

    this.log.drawSeparator()
    info('Server', pkg.name)
    info('Version', `v${pkg.version}`)
    info('Environment', this.environment)
  }

  get app(): Express {
    return this._app
  }

  get server(): http.Server {
    return this._server
  }

  set routePrefix(prefix: string) {
    prefix = prefix.startsWith('/') ? prefix : '/' + prefix
    prefix = prefix.endsWith('/')
      ? prefix.substring(0, prefix.length - 1)
      : prefix
    this._routePrefix = prefix
  }

  get routePrefix() {
    return this._routePrefix
  }

  private async configurePlugins() {
    this.log.info('Configuring plugins...')

    for (let i in this.plugins) {
      const pluginClass = this.plugins[i]
      this.log.debug(chalk.bold('Plugin:') + ' ' + pluginClass.name)
      const plugin = Container.get(pluginClass) as Plugin
      await plugin.init()
    }
  }

  private configureMiddleware() {
    const log = new LoggerService('Router')
    this._app.use(Logger.HttpLogger)
    this._app.use(express.json())
    this._app.use(helmet())

    for (let i in this.middleware) {
      this._app.use(this.middleware[i])
    }
  }

  public async setup(): Promise<void> {
    await this.configurePlugins()
    this.configureMiddleware()
    this.bootstrapControllers()
    this._app.use(this.getErrorHandler())
  }

  public start(callback: () => void = () => {}): Promise<void> {
    return new Promise(async (resolve) => {
      await this.setup()

      this._server = this._app.listen(this._app.get('port'), () => {
        this.log.drawSeparator()
        this.log.info(`🚀 Server running on port ${this._app.get('port')}`)
        callback()
        resolve()
      })
    })
  }

  private bootstrapControllers() {
    this.log.info('Bootstrapping controllers...')
    for (let i in this.controllers) {
      this.setupControllerClass(this.controllers[i])
    }

    this._app.use('*', (req: Request, res: Response, next: NextFunction) =>
      next(new NotFound())
    )
  }

  private setupControllerClass<T>(classController: T): void {
    try {
      const controllerData = ControllerUtils.get(classController)

      for (let i in controllerData.controllers) {
        const controller = controllerData.controllers[i]
        try {
          const controllerMeta = ControllerUtils.getController(
            controllerData,
            controller,
            this.routePrefix,
            this._guards
          )
          this.log.debug(
            chalk.bold('Controller loaded:') +
              ` ${controllerMeta.method.toUpperCase()} ${controllerMeta.path}`
          )

          this._app[controllerMeta.method](
            controllerMeta.path,
            controllerMeta.handler
          )
        } catch (e) {
          console.log(e)
          console.log('???')
        }
      }
    } catch (ignore) {
      this.log.error('Invalid controller:', (classController as any).name)
      this.log.error(ignore)
    }
  }

  private getErrorHandler(): any {
    return (err: any, req: Request, res: Response, next: NextFunction) => {
      let httpErr: HttpValidationError | HttpError
      if (err instanceof TypeError) {
        httpErr = new InternalServerError()
      } else if (!err.status) {
        httpErr = new InternalServerError()
      } else {
        httpErr = err
      }

      if (this.isDev() && httpErr instanceof InternalServerError) {
        this.log.drawSeparator(chalk.red)
        this.log.error(err, chalk.red('\n|\nV'))
      }

      res.status(httpErr.status).json({
        status: httpErr.status || 500,
        message: httpErr.message,
        errors: httpErr.errors
      })
    }
  }

  public enableCors() {
    this._app.use((req: Request, res: Response, next: NextFunction) => {
      res.setHeader('Access-Control-Allow-Origin', '*')
      res.setHeader('Access-Control-Allow-Credentials', 'true')
      res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT')
      res.setHeader(
        'Access-Control-Allow-Headers',
        'Access-Control-Allow-Origin,Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,Authorization'
      )
      next()
    })
  }

  private isDev() {
    return this.environment === Env.DEV
  }

  private get environment(): Env {
    switch (process.env.NODE_ENV) {
      case 'development':
        return Env.DEV
      case 'test':
        return Env.TEST
      case 'production':
        return Env.PROD
      default:
        this.log.error(chalk.bold('Unknown environment:'), process.env.NODE_ENV)
        this.log.warn(
          chalk.bold('Allowed environments:'),
          [Env.DEV, Env.PROD, Env.TEST].join(', ')
        )
        this.shutdown()
        throw new Error()
    }
  }

  public shutdown() {
    this.log.error('Server interrupted...')
    this.log.drawSeparator()
    process.exit(1)
  }
}

export const webServer = Container.get(WebServer) as WebServer

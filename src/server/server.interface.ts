import { Handler } from 'express'
import { Guard } from '../guards/guard.interface'
import { Plugin } from '../plugins/plugin.interface'
import { Express } from 'express'
import { Server } from 'http'
import { Interface } from 'readline'

// export class GenericClass {
//   constructor() {}
// }

export interface IController {}

export interface IWebServer {
  app: Express
  server: Server

  start(callback?: Function): Promise<void>
  enableCors(): void
  shutdown(): void

  routePrefix: string

  guards: (new (...args: any[]) => Guard)[]
  plugins: (new (...args: any[]) => Plugin)[]
  middleware: Handler[]
  controllers: Array<new (...args: any[]) => IController>
}

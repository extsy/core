export enum Env {
  DEV = 'development',
  PROD = 'production',
  TEST = 'test'
}
